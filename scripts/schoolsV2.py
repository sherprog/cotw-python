routeText = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")

servings =[]
numLines = 0
for line in routeText:
    if (line.find("Serving") != -1):
        line = line.strip('\n\r ')
        servings.append(line)
        
routeText.close()

servings.sort()

#define a dictionary to hold our school counts
schools = {"SHS" : 0, "JH" : 0}

for line in servings:
    if (line.find("SHS") != -1) or (line.find("HS") != -1):
        # when we find SHS, add one to its count
        schools["SHS"] += 1

# print out the counts for each school
for k, v in schools.items():
    print(k, ": ", v)
