# open our input and output files (note the different 'mode' parameters -- important!)
routeTextIn = open("<your path to>\Combined-Bus-Routes-2016-2017-1.txt", "r")
routeTextOut = open("<your path to>\cleanRoute.txt", "w+")

# loop through the input file, clean up lines as we go along, and write the output file
numLinesIn = 0
numLinesOut = 0
for line in routeTextIn:
    numLinesIn += 1
	# remove front or back whitespace characters, including some unusual 
	# page feed characters, from the line
    line = line.strip('\n\r\f')
	# if that does not leave the line blank, write out the cleaned-up line
    if len(line) > 0:     
        routeTextOut.write (line + '\n')
        numLinesOut += 1

# close input and output
routeTextIn.close()
routeTextOut.close()

# report on what we've done
print("Number of lines read in:", numLinesIn)
print("Number of lines written out:", numLinesOut)
