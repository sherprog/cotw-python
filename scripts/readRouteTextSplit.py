# open the bus routes text file
routeText = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")

# The file object can be used as an 'iterator' in a simple for loop
numLines = 0
for line in routeText:
    if (line.find("BUS STOP LOCATION") != -1):
        lineList = line.split(" ")
        print(lineList[3], " ", lineList[4])
        numLines += 1

# tell us how many lines were printed
print("Number lines printed:", numLines)

# close the file before we terminate
routeText.close()
