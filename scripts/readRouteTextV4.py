# open the bus routes text file
routeText = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")

# When we read through the file,
# we can find and count specific types of lines
numBusStopLocationLines = 0
for line in routeText:
    line = line.strip(' \n\r')
    if (line.find("BUS STOP LOCATION") != -1):
        print (len(line), ":", line)
        numBusStopLocationLines += 1
        lineParts = line.split(" ")
        print (lineParts[3], lineParts[4])

# tell us how many lines were printed
print("Number bus stop location lines found:", numBusStopLocationLines)

# close the file before we terminate
routeText.close()
