import csv
import pprint
import re

######################################################################
## define functions
######################################################################

# pull data out of lines that look roughly like
# BUS STOP LOCATION Harvey Purcella bus #1 AM
# but there's not as much consistency as one would like
def parseBusStopLocationLine(line, currentBusNumList) :
	# print(currentBusNumList);
	
	# apply some fixup magics -- not elegant but neither is the doc with which we have to work
	# We could test for the search text being there before we try to do the replace
	# but that's twice the work and the replace does no harm if the search text is not found
	line = line.replace("bus #1", "1")
	line = line.replace("#1PM", "1 PM")
	line = line.replace("PM1", "PM 1")
	line = line.replace("BUS STOP Valerie", "BUS STOP LOCATION Valerie")
	
	
	busNum = 0
	ampm = ""
	wordList = line.split()
	driver = wordList[3] + " " + wordList[4]
	if (driver == "Scott Williams") :
		busNum = '2'
	elif (driver == "Ron Maixner") :
		busNum = '24'
	else :
		if (wordList[5] == "AM" or wordList[5] == "PM") :
			ampm = wordList[5]
			busNum = wordList[6]
		elif (wordList[6] == "AM" or wordList[6] == "PM") :
			ampm = wordList[6]
			busNum = wordList[5]
			
	if (ampm == "") :
		# If the line doesn't have an explicit am/pm notation, fall back to
		# just seeing if this bus number is already in the big list or not.
		# If not, we say it is AM. If so, we say it is PM
		if (int(busNum) in currentBusNumList) :
			ampm = "PM"
		else :
			ampm = "AM"
			
	routeInfo = {}
	routeInfo["number"] = busNum 
	routeInfo["driver"] = driver 
	routeInfo["ampm"] = ampm
	routeInfo["schools"] = []
	routeInfo["amStops"] = []
	routeInfo["pmStops"] = []
	# print("parsed route info: ", routeInfo);
	return routeInfo
	
def addBusRouteToMasterList(oneBusRoute, stops, allRoutes) :
	if "number" in oneBusRoute : 
		# add this route to our list of routes and start a new, clean route dictionary
		# we need to know ampm here but don't need that entry in the dictionary after this
		intBusNum = int(oneBusRoute["number"])
		ampm = oneBusRoute.pop("ampm")
		if (ampm == "AM") :
			# print("about to add bus route with am stops to list", oneBusRoute["number"])
			oneBusRoute["amStops"] = stops 
			allRoutes[intBusNum] = oneBusRoute
			# print(stops)
		else :
			# print("about to add pm stops to previous route", oneBusRoute["number"])
			# tuck the pm stops into the existing busroute
			allRoutes[intBusNum]["pmStops"] = stops 
			# print(stops)

def getSchoolName(line):
	wordList = line.split()
	name = wordList[1]
	if name == "H":
		name = "Coffeen"
	if name == "SJHS":
	    name = "JHS"
	return name
	
def getCleanAddress(line) :
	cruftList = ["(", "x's", "@ alley", "@ mail", "on corner", " by ", " - ", "north side", "transfer to" ]
	cleanLine = line
	for cruft in cruftList :
		parenLocation = line.find(cruft)
		if (parenLocation != -1) :
			cleanLine = line[:parenLocation]
			break
	return cleanLine
	
	

timePattern = re.compile(r'^\d:\d\d')

######################################################################
## main processing loop
######################################################################
routeText = open("c:/cotw/cotw-python/sources/cleanRoute.txt", "r")
#routeText = open("c:/cotw/cotw-python/sources/shortRoute.txt", "r")

numBSLLines = 0
numArrive = 0
numStops = 0
stops = []
oneBusRoute = {}
allRoutes = {}
time = ""
for line in routeText :
	line = line.strip('\n\r\t ')
	
	if (line.find("BUS STOP ") != -1) :
		#print(line)
		# we have the start of a new bus route
		# Add the old route, if any, to our master list
		addBusRouteToMasterList(oneBusRoute, stops, allRoutes)
		numBSLLines += 1
		oneBusRoute = parseBusStopLocationLine(line, allRoutes.keys())
		stops = []
		time = ""
	elif (timePattern.search(line)) :
		time = line
	elif (time != "") :
		# next line after a time is assumed to be a stop
		stopInfo = {
			"time" : time,
			"address" : line
			}
		numStops += 1
		stops.append(stopInfo)
		time = ""
			
	# in addition to being processed as a stop, we use the 
	#'arrive' lines as our best proxy for which schools 
	# the bus serves
	if ((line.find("Arrive") != -1) or (line.find("Ar/Lv") != -1)) :
		schoolName = getSchoolName(line)
		oneBusRoute["schools"].append(schoolName)
		numArrive += 1
# end for

# be sure to pick up that pesky last entry that is so easy to miss
addBusRouteToMasterList(oneBusRoute, stops, allRoutes)
               
routeText.close()

print("Number of BUS STOP LOCATION lines:", numBSLLines)
print("Number of Arrive lines:", numArrive)
print("Number of routes: ", len(allRoutes.values()))
print("Number of stops: ", numStops)

dumpFilePath = "c:/cotw/cotw-python/sources/routeDump.txt"
routeDumpOut = open(dumpFilePath, "w+")
ppDump = pprint.PrettyPrinter(indent=2, width=80, stream=routeDumpOut)
ppDump.pprint(allRoutes)
routeDumpOut.flush()
routeDumpOut.close()

print("Please review the overall results in: ", dumpFilePath)

# create one big array of busNumbers, times, and addresses
allStopList = []
geoCodeList = []
schoolRouteList = {}
busNum = ""
ampm = ""
time = ""
address = ""
for busNum, busInfo in allRoutes.items() :
	# TODO: Note we are only doing AM stops right now
	ampm = "AM"
	stopList = busInfo["amStops"]
	for stop in stopList :
		time = stop["time"]
		address = stop["address"]
		stopTuple = (busNum, ampm, time, address)
		allStopList.append(stopTuple)
		cleanAddress = getCleanAddress(address)
		geocodeTuple = (cleanAddress, "Sheridan", "WY")
		geoCodeList.append(geocodeTuple)
	# let's put a blank line between the bus routes 
	dummyTuple = ("", "", "")
	geoCodeList.append(dummyTuple)
	
	# TODO: school names are not consistent and need to be 
	# cleaned up -- we had to join lists by hand the first time
	schoolList = busInfo['schools']
	for schoolName in schoolList :
		if schoolName not in schoolRouteList.keys() :
			#print(schoolName, "found first time")
			schoolRouteList[schoolName] = []
		schoolRouteList[schoolName].append(busNum)

#print(schoolRouteList)
		
# Write the master stop list out as a csv file
stopFilePath = "c:/cotw/cotw-python/sources/stopList.csv"
with open(stopFilePath, 'w+', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(allStopList)
print("Please review the stop list in: ", stopFilePath)

# Write the geoCode list out as a new csv file 
stopFilePath = "c:/cotw/cotw-python/sources/geoCodeList.csv"
with open(stopFilePath, 'w+', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(geoCodeList)
print("Please review the geoCode list in: ", stopFilePath) 

#Write out the list of schools and their bus routes
schoolListFilePath = "c:/cotw/cotw-python/sources/schoolRouteList.csv"
with open(schoolListFilePath, 'w+', newline='') as f:
	writer = csv.writer(f)
	for school in schoolRouteList :
		schoolRouteList[school].insert(0,school)
		writer.writerow(schoolRouteList[school])

print("Please review the school route list input list in: ", schoolListFilePath)



	
	
	
