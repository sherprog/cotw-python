routeTextIn = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")
routeTextOut = open("..\sources\cleanRoute.text", "w+")

numLinesIn = 0
numLinesOut = 0
for line in routeTextIn:
        numLinesIn += 1
        line = line.strip('\n\r\t\f ')
        if (len(line) > 0):
                routeTextOut.write(line + '\n')
                numLinesOut += 1

routeTextIn.close()
routeTextOut.close()

print("Number of lines read in:", numLinesIn)
print("Number of lines written out:", numLinesOut)
