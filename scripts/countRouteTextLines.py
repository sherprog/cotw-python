# open the bus routes text file
routeText = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")

# read each line in the file and count it
numLines = 0
while routeText.readline():
    numLines += 1

# tell us how many lines you read
print("Number lines read:", numLines)

# close the file before we terminate
routeText.close()
