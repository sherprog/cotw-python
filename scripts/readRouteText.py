# open the bus routes text file
routeText = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")

# The file object can be used as an 'iterator' in a simple for loop
# that gives you each line in the file, one at a time
numLines = 0
for line in routeText:
    print (line)
    numLines += 1

# tell us how many lines were printed
print("Number lines printed:", numLines)

# close the file before we terminate
routeText.close()
