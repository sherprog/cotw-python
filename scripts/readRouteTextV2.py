# open the bus routes text file
routeText = open("..\sources\Combined-Bus-Routes-2016-2017-1 (1).txt", "r")

# The file object can be used as an 'iterator' in a simple for loop
numLines = 0
for line in routeText:
    line = line.strip(' \n\r')
    if (len(line) > 0):
        print (len(line), ":", line)
        numLines += 1

# tell us how many lines were printed
print("Number lines printed:", numLines)

# close the file before we terminate
routeText.close()
